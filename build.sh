# @Author: Shiv Chawla
# @Date:   2017-10-06 17:42:29
# @Last Modified by:   Shiv Chawla
# @Last Modified time: 2017-10-10 22:45:34
#!/bin/bash
cp -R bower_components_modified/.  bower_components/
polymer build