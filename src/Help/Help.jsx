import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'; 
import { Collapse, Tabs, Spin, Icon, Breadcrumb, Input } from 'antd';
import axios from 'axios';
import Utils from './../Utils';
import JsxParser from 'react-jsx-parser';
import Highlight from './../Community/HighLight/Highlight.jsx';
import 'highlight.js/styles/atelier-cave-light.css';
import Abc from './abc.jsx'; //WTF is this component?
import Loading from 'react-loading-bar'
import 'react-loading-bar/dist/index.css'H


class Help extends Component {

  _mounted = false;
  cancelHelpGet = undefined;
  cancelTutorialGet = undefined;

  lastHighlightedNode = undefined;

  constructor(props){
  	super(); 
  	this.state = {
      'helpLoading': true,
      'tutorialLoading': true,
      'helpData': '',
      'helpLinks': '',
      'tutorialData': '',
      'searchString': ''
  	};


    this.getHelpData = () => {
      axios(Utils.getHelpUrl(), {
        cancelToken: new axios.CancelToken( (c) => {
          // An executor function receives a cancel function as a parameter
          this.cancelHelpGet = c;
        })
      })
        .then((response) => {
            this.updateState({
              'helpData': response.data.Help,
              'helpLoading': false
            });
            this.cancelHelpGet = undefined;
          })
          .catch((error) => {
            this.updateState({
              'helpData': error,
              'helpLoading': false
            });
            this.cancelHelpGet = undefined;
          });
    };

    this.getTutorialData = () => {
      axios(Utils.getTutorialUrl(), {
        cancelToken: new axios.CancelToken( (c) => {
          // An executor function receives a cancel function as a parameter
          this.cancelTutorialGet = c;
        })
      })
        .then((response) => {
            this.updateState({
              'tutorialData': response.data.tutorials,
              'tutorialLoading': false
            });
            this.cancelTutorialGet = undefined;
          })
          .catch((error) => {
            this.updateState({
              'tutorialData': error,
              'tutorialLoading': false
            });
            this.cancelTutorialGet = undefined;
          });
    }

    this.scrollTo = (index) => {
      const testNode = document.getElementById(index);
      const linkNode = document.getElementById(index+'_linkElement'); 
      if(this.lastHighlightedNode){
        this.removeClass(this.lastHighlightedNode, 'active');
      }
      this.lastHighlightedNode = linkNode;
      if (testNode){
        document.getElementById("basicDiv").scrollTop = (testNode.offsetTop - 80 );
      }
      this.addClass(linkNode, 'active');
    }

    this.addClass = (element, name) => {
        let arr = element.className.split(" ");
        if (arr.indexOf(name) === -1) {
            element.className += " " + name;
        }
    }

    this.removeClass = (element, name) => {
        let arr = element.className.split(" ");
        if (arr.indexOf(name) > -1) {
            arr.splice(arr.indexOf(name), 1);
        }
        element.className = arr.join(" ");
    }

    this.updateState = (data) => {
      if (this._mounted){
        this.setState(data);
      }
    }

    this.onSearchKeyPressed = e => {
      if (e.key === 'Escape'){
        e.target.value = "";
        this.updateState({'searchString': ''});
      }
    }

    this.searchHelp = value => {
      this.updateState({'searchString': value});
    }

  }

  componentDidMount(){
    this._mounted = true;
    if (this._mounted){
      this.updateState({'helpLoading': true});
      setTimeout(() => {
        this.getHelpData();
        this.getTutorialData();
      }, 100);
    }
  }

  componentWillUnmount(){
    this._mounted = false;
    if(this.cancelHelpGet){
      this.cancelHelpGet();
    }
    if (this.cancelTutorialGet){
      this.cancelTutorialGet()
    }
  }


  render() {

    const TabPane = Tabs.TabPane;
    const antIconLoading = <Icon type="loading" style={{ fontSize: 34 }} spin />;

    const getTutorialDiv = () =>{
      if (this.state.tutorialLoading){
        return (
          <div style={{'display': 'flex',
            'alignItems': 'center', 'justifyContent': 'center',
            'minHeight': '300px'}}>
            <Spin indicator={antIconLoading} />
          </div>
        );
      }else{
        const Panel = Collapse.Panel;
        let data = "<Collapse accordion>";
        for(let i=0; i<this.state.tutorialData.length; i++){
          data = data + "<Panel forceRender={true} header='" + this.state.tutorialData[i].header + "' key='"+i+"'>" 
          + "<div style='padding-left: 15px'>" + this.state.tutorialData[i].content + "</div></Panel>";
        }
        data = data + "</Collapse>";
        return(
          <div style={{'height': '100%', 'overflowY': 'auto', 'paddingBottom': '80px'}}
               id="tutorialDiv">
            <Highlight className='julia'>
              <JsxParser
                jsx={data}
                showWarnings = {true}
                components={{ Collapse, Panel }}
              />
            </Highlight>
              
          </div>
        );
      }
    }

    const getHelpDiv = () =>{
      if (this.state.helpLoading){
        return (
          <div style={{'display': 'flex',
            'alignItems': 'center', 'justifyContent': 'center',
            'minHeight': '300px'}}>
            <Spin indicator={antIconLoading} />
          </div>
        );
      }else{

        let data = "";
        const helpDataArr = [];
        const lengthy = this.state.helpData.length;
        for(let i=0; i<lengthy; i++){
          const dty = this.state.helpData[i];
          if (dty.header.value.toLowerCase().indexOf(this.state.searchString.toLowerCase()) > -1){
            if (dty.header.size === 'Big'){
              helpDataArr.push(<Abc paddingLeft={10} key={i} eventHandler={this.scrollTo} title={dty.header.value} valueSelf={i}></Abc>);
            }else if(dty.header.size === 'Normal'){
              helpDataArr.push(<Abc paddingLeft={30} key={i} eventHandler={this.scrollTo} title={dty.header.value} valueSelf={i}></Abc>);
            }else if(dty.header.size === 'Medium'){
              helpDataArr.push(<Abc paddingLeft={50} key={i} eventHandler={this.scrollTo} title={dty.header.value} valueSelf={i}></Abc>);
            }
          }
          
          if (dty.img_src && dty.img_src.length > 0){
            data = data + "<div id='" + i + "'>" + dty.text + "<div style='display: flex; justify-content: center;'><img src='/assets/resources/images/"+dty.img_src+"' alt='hello' /></div></div>";
          }else{
            data = data + "<div id='" + i + "'>" + dty.text + "</div>";
          }
        }

        return(
          <div style={{'height': '100%', 'display': 'flex'}}>
            <div style={{'height': '100%', 'overflowY': 'auto',
                'flex': '1', 'paddingBottom': '50px'}}
                 id="basicDiv">
              <Highlight className='julia'>
                <JsxParser
                  jsx={data}
                  showWarnings={true}
                />
              </Highlight>
                
            </div>
            <div style={{'height': '100%', 'paddingBottom': '10px', 'minWidth': '300px', 'overflowY': 'auto'}}>
              <div style={{'padding': '0px 10px 10px 10px'}}>
                <Input.Search
                    placeholder="Search"
                    onSearch={value => this.searchHelp(value)}
                    enterButton
                    onKeyDown = {this.onSearchKeyPressed}
                  />
              </div>
              {helpDataArr}
            </div>
          </div>
        );
      }
    }

    const getBreadCrumbHelp = () => {
        return(
          <Breadcrumb separator=">" className="location-breadcrumb">
              <Breadcrumb.Item className="last">Help</Breadcrumb.Item>
          </Breadcrumb>
        );
    }

    const getTotalDiv = () => {
      if (!this.state.helpLoading && !this.state.tutorialLoading){
        return (
          <div className="thread-view-div" style={{'width': '100%', 'height': 'calc(100vh - 64px)', 'overflowY': 'hidden'}}>
            <div className="card helpMainDiv" style={{'width': '100%', 'background': 'white',
              'padding': '5px 25px 5px 25px', 'height': '100%'}}>
              <div style={{'display': 'flex', 'margin': '0px 0px 5px 10px'}}>
                <div>
                  <h2 style={{'color': '#3c3c3c', 'fontWeight': 'normal', 'margin': '0px'}}>Help</h2>
                  {getBreadCrumbHelp()}
                </div>
              </div>
              <Tabs animated={false} defaultActiveKey="1" style={{'height': 'calc(100% - 35px)'}} className="help-tabs">
                <TabPane tab="BASIC" key="1" style={{
                  'height': 'calc(100% - 50px)'
                }}>
                  {getHelpDiv()}
                </TabPane>
                <TabPane tab="TUTORIALS" key="2" style={{
                  'height': 'calc(100% - 50px)',
                  'overflowY': 'auto'
                }}>
                  {getTutorialDiv()}
                </TabPane>
              </Tabs>
            </div>
          </div>
        );
      }
    }

    return (
      <React.Fragment>
        <div className="main-loader">
          <Loading
            show={this.state.helpLoading || this.state.tutorialLoading}
            color="teal"
            showSpinner={false}
          />
        </div>
        {getTotalDiv()}
      </React.Fragment>
    );
  }
}

export default withRouter(Help);
