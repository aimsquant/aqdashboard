import React, { Component } from 'react';
import {Icon} from 'antd';
class Abc extends Component {
  constructor(props){
  	super()
  }
  render(){
  	return(
  		<div style={{'paddingLeft': this.props.paddingLeft + 'px', 'display': 'flex', 'alignItems': 'center'}}>
  			<Icon style={{'fontSize': '6px', 'paddingRight': '5px'}} type="play-circle" />
  			<a id={this.props.valueSelf + '_linkElement'} className="help-basic-anchor" onClick={()=>this.props.eventHandler(this.props.valueSelf)}>{this.props.title}</a>
  		</div>
  	);
  }
}

export default Abc;