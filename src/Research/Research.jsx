import React, { Component } from 'react';
import Utils from './../Utils';
import { Spin, Icon, Row, Col, Checkbox, Button, List, Modal, Input } from 'antd';
import axios from 'axios';
import { withRouter, Link } from 'react-router-dom';
import Moment from 'react-moment';
import NewStartegy from './NewStrategy/NewStrategy.jsx';
import { Breadcrumb } from 'antd';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';


class Research extends Component {

  _mounted = false;
  cancelGetStrategies = undefined;
  forwardTestsStopped = 0;
  strategiesDeleted = 0;

  constructor(props){
  	super();
  	this.state = {
      'loading': true,
      'selectedStrategies': [],
      'selectedLiveTests': [],
      'allLiveTestsSelected': false,
      'strategies': [],
      'allSelected': false,
      'stopForwardTestLoading': false,
      'deleteStrategiesLoading': false,
      'searchBoxOpen': false,
      'oldSearchString': '',
      'showNewStartegyDiv': false
  	};
    this.updateState = (data) => {
      if (this._mounted){
        this.setState(data);
      }
    }
    this.getAllStrategies = (searchString) =>{
      let urlString = Utils.getBaseUrl() + '/strategy';
      if (searchString && searchString.trim().length > 0){
        urlString = urlString + "?search="+encodeURIComponent(searchString);
      }
      axios(urlString, {
        cancelToken: new axios.CancelToken( (c) => {
          // An executor function receives a cancel function as a parameter
          this.cancelGetStrategies = c;
        }),
        'headers': Utils.getAuthTokenHeader()
      })
        .then((response) => {
            this.updateState({'strategies': response.data, 'loading': false});
            this.cancelGetStrategies = undefined;
          })
          .catch((error) => {
            Utils.checkForInternet(error, this.props.history);
            if (error.response) {
              if (error.response.status === 400 || error.response.status === 403) {
                this.props.history.push('/forbiddenAccess');
              }
              Utils.checkErrorForTokenExpiry(error, this.props.history, this.props.match.url);
            }
            this.updateState({
              'strategies': [],
              'loading': false
            });
            this.cancelGetStrategies = undefined;
          });
    }

    this.strategyCheckBoxChange = (state, strategyId) => {
      let startegyIds = JSON.parse(JSON.stringify(this.state.selectedStrategies));
      let indexOfId = startegyIds.indexOf(strategyId);
      if(state){
        if (indexOfId === -1){
          startegyIds.push(strategyId);
        }
        if(startegyIds.length === this.state.strategies.length){
          this.updateState({'selectedStrategies': startegyIds,
          'allSelected': true});
        }else{
          this.updateState({'selectedStrategies': startegyIds});
        }
      }else if(indexOfId >= 0){
        startegyIds.splice(indexOfId, 1);
        this.updateState({'selectedStrategies': startegyIds,
        'allSelected': false});
      }
      
    }

    this.liveTestCheckBoxChange = (state, forwardTestId) => {
      let liveTestIds = JSON.parse(JSON.stringify(this.state.selectedLiveTests));
      let indexOfId = liveTestIds.indexOf(forwardTestId);
      if(state){
        if (indexOfId === -1){
          liveTestIds.push(forwardTestId);
        }
        this.updateState({'selectedLiveTests': liveTestIds});
      }else if(indexOfId >= 0){
        liveTestIds.splice(indexOfId, 1);
        this.updateState({'selectedLiveTests': liveTestIds,
        'allLiveTestsSelected': false});
      }
      
    }

    this.allStrategiesCheckboxChange = (e) =>{
      let selectedStrategies = [];
      if (e.target.checked){
        for(let i=0; i<this.state.strategies.length; i++){
          selectedStrategies.push(this.state.strategies[i]._id);
        }
      }
      this.updateState({'selectedStrategies': selectedStrategies,
        'allSelected': e.target.checked});
    }

    this.allLiveTestsCheckBoxChange = (e) =>{
      let selectedLiveTests = [];
      if (e.target.checked){
        for(let i=0; i<this.state.strategies.length; i++){
          if (this.state.strategies[i].forwardtest &&
            this.state.strategies[i].forwardtest._id){
            selectedLiveTests.push(this.state.strategies[i].forwardtest._id);
          }
        }
      }
      this.updateState({'selectedLiveTests': selectedLiveTests,
        'allLiveTestsSelected': e.target.checked});
    }

    this.updateLiveTestsState = () => {
      if (this.forwardTestsStopped === this.state.selectedLiveTests.length){
        this.forwardTestsStopped = 0;
        let strategies = [];
        for(let i=0; i<this.state.strategies.length; i++){
          let strategyLocal = JSON.parse(JSON.stringify(this.state.strategies[i]));
          if (strategyLocal.forwardtest && this.state.selectedLiveTests.indexOf(strategyLocal.forwardtest._id) >= 0){
            strategyLocal.forwardtest.active = false;
          }
          strategies.push(strategyLocal);
        }
        this.updateState({
          'stopForwardTestLoading': false,
          'selectedLiveTests': [],
          'strategies': strategies,
          'allLiveTestsSelected': false
        });
      }
    }

    this.stopforwardTests = (forwardTestId) => {
      axios({
              method: 'PUT',
              url: Utils.getBaseUrl() + '/forwardtest/'+forwardTestId+'?active=false',
            'headers': Utils.getAuthTokenHeader()
            })
          .then((response) => {
            this.forwardTestsStopped = this.forwardTestsStopped + 1;
            this.updateLiveTestsState();
          })
          .catch((error) => {
            this.forwardTestsStopped = this.forwardTestsStopped + 1;
            this.updateLiveTestsState();
          });
    }

    this.stopAllSelectedForwardTests = () => {
      if (this.state.selectedLiveTests && this.state.selectedLiveTests.length > 0){
        this.updateState({'stopForwardTestLoading': true});
        this.forwardTestsStopped = 0;
        for(let i=0; i<this.state.selectedLiveTests.length; i++){
          this.stopforwardTests(this.state.selectedLiveTests[i]);
        }
      }
    }

    this.updateStrategiesDeleted = () => {
      if (this.strategiesDeleted === this.state.selectedStrategies.length){
        this.strategiesDeleted = 0;
        let strategies = [];
        for(let i=0; i<this.state.strategies.length; i++){
          let strategyLocal = JSON.parse(JSON.stringify(this.state.strategies[i]));
          if (this.state.selectedStrategies.indexOf(strategyLocal._id) === -1){
            strategies.push(strategyLocal);
          }
        }
        this.updateState({
          'deleteStrategiesLoading': false,
          'selectedStrategies': [],
          'strategies': strategies,
          'allSelected': false
        });
      }
    }

    this.deleteStrategy = (strategyId) => {
      axios({
              method: 'DELETE',
              url: Utils.getBaseUrl() + '/strategy/'+strategyId,
             'headers': Utils.getAuthTokenHeader()
            })
          .then((response) => {
            this.strategiesDeleted = this.strategiesDeleted + 1;
            this.updateStrategiesDeleted();
          })
          .catch((error) => {
            this.strategiesDeleted = this.strategiesDeleted + 1;
            this.updateStrategiesDeleted();
          });
    }

    this.deleteAllSelectedStrategies = () => {
      if (this.state.selectedStrategies && this.state.selectedStrategies.length > 0){
        this.updateState({'deleteStrategiesLoading': true});
        this.strategiesDeleted = 0;
        for(let i=0; i<this.state.selectedStrategies.length; i++){
          this.deleteStrategy(this.state.selectedStrategies[i]);
        }
      }
    }


    this.showDeleteConfirm = () => {
      Modal.confirm({
        title: "Are you sure you want to delete?",
        content: this.state.selectedStrategies.length + ' strategies will be deleted.',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: () => {
          this.deleteAllSelectedStrategies();
        },
        onCancel: () => {
        },
      });
    }


    this.showStopConfirm = () => {
      Modal.confirm({
        title: "Are you sure you want to stop?",
        content: this.state.selectedLiveTests.length + ' live tests will be stopped.',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: () => {
          this.stopAllSelectedForwardTests();
        },
        onCancel: () => {
        },
      });
    }

    this.onSearchKeyPressed = event => {
      if (event.key === 'Escape'){
        event.target.value = "";
        this.searchStrategies("");
      }
    }

    this.searchStrategies = (value) => {
      if (this.state.oldSearchString !== value){
        this.updateState({
          'strategies': [],
          'loading': true,
          'oldSearchString': value
        })
        this.getAllStrategies(value);
      }
    }

  }

  componentDidMount(){
    this._mounted = true;
    if (!Utils.isLoggedIn()){
      Utils.goToLoginPage(this.props.history, this.props.match.url);
    }else{
      this.updateState({'loading': true});
      if (this._mounted){
        this.getAllStrategies(undefined);
      }
    }
  }

  componentWillUnmount() {
    this._mounted = false;
    if (this.cancelGetStrategies){
      this.cancelGetStrategies();
    }
  }

  render() {

    const antIconLoading = <Icon type="loading" style={{ fontSize: 34 }} spin />;

    const getDeleteButton = () =>{
      if (this.state.deleteStrategiesLoading){
        return (
          <div style={{'display': 'flex',
            'alignItems': 'center', 'justifyContent': 'center',
            'minWidth': '100px'}}>
            <Spin indicator={antIconLoading} />
          </div>
        );
      }else if (this.state.selectedStrategies && this.state.selectedStrategies.length > 0){
        return (
          <Button type="danger" ghost onClick={
            ()=>{
              this.showDeleteConfirm();
            }
          }>
            <Icon type="delete" />Delete Selected
          </Button>
        );
      }else{
        return (
          <Button type="danger" ghost disabled>
            <Icon type="delete" />Delete Selected
          </Button>
        );
      }
    }

    const getDeleteButtonLiveTests = () => {
      let showStopButton = false;
      if (this.state.selectedLiveTests && this.state.selectedLiveTests.length > 0){
        showStopButton = true;
        for(let i=0; i<this.state.strategies.length; i++){
          if (this.state.strategies[i].forwardtest &&
              this.state.strategies[i].forwardtest._id &&
              this.state.selectedLiveTests.indexOf(this.state.strategies[i].forwardtest._id) > -1 &&
              !this.state.strategies[i].forwardtest.active){
            showStopButton = false;
          }
        }
      }
      if (this.state.stopForwardTestLoading){
        return (
          <div style={{'display': 'flex',
            'alignItems': 'center', 'justifyContent': 'center',
            'minWidth': '100px'}}>
            <Spin indicator={antIconLoading} />
          </div>
        );
      }else if (showStopButton){
        return (
          <Button type="danger" ghost onClick={
            ()=>{
              this.showStopConfirm();
            }
          }>
            <Icon type="close-square" />Stop selected
          </Button>
        );
      }else{
        return (
          <Button type="danger" ghost disabled>
            <Icon type="close-square" />Stop selected
          </Button>
        );
      }
    }

    const getBackTestsButton = (numOfBacktests, strategyId) => {
      if (numOfBacktests > 0){
        return (
          <Link to={'/research/backtests/'+strategyId}>
            <Button type="primary" ghost>
              Backtests ({numOfBacktests})
            </Button>
          </Link>
        );
      }else{
        return (
          <Button type="primary" ghost disabled>
            Backtests (0)
          </Button>
        );
      }
    }

    const getLiveTestStatus = (active, error, forwardTestId) => {
      if (error){
        return (
          <p style={{'backgroundColor': '#bd362f',
            'fontSize': '12px', 'fontWeight': '700',
            'color': 'white', 'padding': '3px'}}>
            ERROR
          </p>
        );
      }else if(active){
        return (
          <p style={{'backgroundColor': '#339933',
            'fontSize': '12px', 'fontWeight': '700',
            'color': 'white', 'padding': '3px'}}>
            RUNNING
          </p>
        );
      }else{
        return (
          <p style={{'backgroundColor': '#bd362f',
            'fontSize': '12px', 'fontWeight': '700',
            'color': 'white', 'padding': '3px'}}>
            STOPPED
          </p>
        );
      }
    }

    const getLiveTestsDiv = () => {
      if (!this.state.loading){
        const liveTests = [];
        for(let i=0; i<this.state.strategies.length; i++){
          if (this.state.strategies[i].forwardtest && 
            this.state.strategies[i].forwardtest._id){
            let liveTestLocal = JSON.parse(JSON.stringify(this.state.strategies[i].forwardtest));
            liveTestLocal['fullName'] = this.state.strategies[i].fullName;
            liveTests.push(liveTestLocal);
          }
        }
        if (liveTests.length > 0){
          return(
            <div style={{'padding': '10px', 'border': '1px solid #e1e1e1',
            'width': '100%', 'marginBottom': '30px'}}>
              <h2 style={{'fontWeight': '700', 'fontSize': '16px', 'margin': '0px'}}>
                LIVE TESTS
              </h2>
              <Row type="flex" align="middle" style={{'marginBottom': '15px'}}>
                <Col span={12}>
                  <Checkbox style={{'fontSize': '18px', 'marginLeft': '15px'}} 
                    onChange={this.allLiveTestsCheckBoxChange}
                    checked={this.state.allLiveTestsSelected}>
                    All Tests
                  </Checkbox>
                </Col>
                <Col span={12} >
                  <div style={{'display': 'flex', 'justifyContent': 'flex-end'}}>
                    {getDeleteButtonLiveTests()}
                  </div>
                </Col>
              </Row>
              <div style={{'padding': '15px',
              'width': '100%'}}>
                <List
                  itemLayout="horizontal"
                  dataSource={liveTests}
                  renderItem={item => (
                    <List.Item onClick={() => 
                      this.props.history.push('/research/forwardtest/'+item.strategy._id+'/'+item._id+'?strategyName='+item.fullName)}
                      className="card-1 research-livetests-listitem">
                      <div style={{'display': 'flex'}}
                        onClick={(e)=>  {e.stopPropagation()}}>
                        {(item.active) ? 
                          (
                            <Checkbox style={{'fontSize': '18px', 'marginLeft': '15px'}} 
                              onChange={(e) => {this.liveTestCheckBoxChange(e.target.checked, item._id)}}
                              checked={(this.state.selectedLiveTests.indexOf(item._id) >= 0) ? true : false}>
                            </Checkbox>
                          ) :
                          (
                            <Checkbox style={{'fontSize': '18px', 'marginLeft': '15px'}} disabled>
                            </Checkbox>
                          )}
                      </div>
                      <div style={{'flex': '1'}}>
                        <Row type="flex" align="middle">
                          <Col md={12} sm={24}>
                            <div style={{'paddingLeft': '15px'}}>
                              <h3>{item.fullName}</h3>
                              <p style={{'fontSize': '12px',
                                'color':'#494949'}}>
                                  Created At: 
                                  <Moment format="DD/MM/YYYY hh:mm A">
                                    {item.createdAt}
                                  </Moment>
                              </p>
                            </div>
                          </Col>
                          <Col md={12} sm={24}>
                            <div style={{'display': 'flex', 'justifyContent': 'flex-end', 'paddingRight': '15px'}}>
                              {getLiveTestStatus(item.active, item.error, item._id)}
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </List.Item>
                  )}
                />
              </div>
            </div>
          );
        }
      }
    }

    const getNewStartegyModal = () => {
      return (
        <Modal
          title=""
          wrapClassName="vertical-center-modal"
          visible={this.state.showNewStartegyDiv}
          footer={null}
          onCancel={() => this.updateState({'showNewStartegyDiv': false})}
        >
          <NewStartegy 
            onCancel={() => this.updateState({'showNewStartegyDiv': false})}
            />
        </Modal>
      );
    }

    const getSearchTextAsNeeded = () => {
      if (this.state.oldSearchString && this.state.oldSearchString.trim().length > 0){
        return (
          <h3>Strategy results for &nbsp;
            <span style={{'color': 'teal'}}>
              "{this.state.oldSearchString}"
            </span>
          </h3>
        );
      }
    }

    const getStrategiesListDiv = () => {
      if (this.state.strategies && this.state.strategies.length > 0){
        return (
          <List
            itemLayout="horizontal"
            dataSource={this.state.strategies}
            renderItem={item => (
              <List.Item>
                <div style={{'display': 'flex'}}>
                  <Checkbox style={{'fontSize': '18px', 'marginLeft': '15px'}} 
                    onChange={(e) => { this.strategyCheckBoxChange(e.target.checked, item._id)}}
                    checked={(this.state.selectedStrategies.indexOf(item._id) >= 0) ? true : false}>
                  </Checkbox>
                </div>
                <div style={{'flex': '1'}}>
                  <Row>
                    <Col md={12} sm={24}>
                      <div style={{'paddingLeft': '15px'}}>
                        <Link to={'/research/strategy/' + item._id}><h3>{item.fullName}</h3></Link>
                        <p style={{'fontSize': '12px',
                          'color':'#494949'}}>
                            Created At: 
                            <Moment format="DD/MM/YYYY hh:mm A">
                              {item.createdAt}
                            </Moment>
                        </p>
                      </div>
                    </Col>
                    <Col md={12} sm={24}>
                      <div style={{'display': 'flex', 'justifyContent': 'flex-end'}}>
                        {getBackTestsButton(item.numBacktests, item._id)}
                      </div>
                    </Col>
                  </Row>
                  <p style={{'paddingLeft': '15px'}}>{item.description}</p>
                </div>
              </List.Item>
            )}
          />
        );
      }else{
        return (
          <div style={{'minHeight': '250px', 'display': 'flex', 'alignItems': 'center',
            'justifyContent': 'center'}}>
            <h2>No Strategies yet!!&nbsp;
              <span onClick={()=>this.updateState({'showNewStartegyDiv': true})}
                style={{'cursor': 'pointer', 'color': 'teal'}}>
                Create New
              </span>
            </h2>
          </div>
        );
      }
    }

    const getStrategiesDiv = () => {
      if (this.state.loading){
        return (
          <div style={{'display': 'flex',
            'alignItems': 'center', 'justifyContent': 'center',
            'minHeight': '300px'}}>
            <Spin indicator={antIconLoading} />
          </div>
        );
      }else{
        
        return (
          <React.Fragment>
            <div style={{'display': 'flex', 'justifyContent': 'flex-end',
              'marginBottom': '15px'}}>
              {(!this.state.searchBoxOpen) ? 
                (<Icon type="search" style={{'border': '1px solid #e1e1e1', 
                'padding': '10px', 'cursor': 'pointer'}}
                onClick={()=>{this.updateState({'searchBoxOpen': true})}}/>)
                :
                (
                  <Input.Search
                    placeholder="Strategy Search"
                    onSearch={value => this.searchStrategies(value)}
                    enterButton
                    style={{'maxWidth': '300px'}}
                    defaultValue={this.state.oldSearchString}
                    onKeyDown = {this.onSearchKeyPressed}
                  />
                )}
            </div>
            {getSearchTextAsNeeded()}
            {getLiveTestsDiv()}
            <div style={{'padding': '10px', 'border': '1px solid #e1e1e1',
              'width': '100%'}}>
              <h2 style={{'fontWeight': '700', 'fontSize': '16px', 'margin': '0px'}}>
                TEST STRATEGIES
              </h2>
              <Row type="flex" align="middle" style={{'marginBottom': '15px'}}>
                <Col span={12}>
                  <Checkbox style={{'fontSize': '18px', 'marginLeft': '15px'}} 
                    onChange={this.allStrategiesCheckboxChange}
                    checked={this.state.allSelected}>
                    All Strategies
                  </Checkbox>
                </Col>
                <Col span={12} >
                  <div style={{'display': 'flex', 'justifyContent': 'flex-end'}}>
                    {getDeleteButton()}
                  </div>
                </Col>
              </Row>
              <div style={{'padding': '15px', 'border': '1px solid #e1e1e1',
              'width': '100%'}}>
                {getStrategiesListDiv()}
              </div>
            </div>
          </React.Fragment>
        );
      }
    }

    const getBreadCrumbAllStrategies = () => {
      return(
        <Breadcrumb separator=">" className="location-breadcrumb">
            <Breadcrumb.Item>Research</Breadcrumb.Item>
            <Breadcrumb.Item className="last">All Strategies</Breadcrumb.Item>
        </Breadcrumb>
      );
    }

    const getTotalDiv = () => {
      if (!this.state.loading){
        return (
          <div className="research-div" style={{'padding': '1% 3% 1% 3%', 'width': '100%', 'minHeight': 'calc(100vh - 70px)'}}>
            <Row style={{'marginBottom': '10px'}} type="flex" align="middle">
              <Col span={12} style={{'display': 'flex', 'alignItems': 'center'}}>
                <div>
                  <h2 style={{'color': '#3c3c3c', 'fontWeight': 'normal',
                    'marginBottom': '0px'}}>All Strategies</h2>
                  {getBreadCrumbAllStrategies()}
                </div>
              </Col>
              <Col span={12} style={{'display': 'flex', 'justifyContent': 'flex-end', 'alignItems': 'center'}}>
                <Button type="primary" style={{'float': 'right'}}
                  onClick={()=>this.updateState({'showNewStartegyDiv': true})}>
                  CREATE NEW
                </Button>
                {getNewStartegyModal()}
              </Col>
            </Row>
            <div className="card" style={{'width': '100%', 'background': 'white',
              'padding': '40px 5% 40px 5%'}}>
              {getStrategiesDiv()}
            </div>
          </div>
        );
      }
    }
    return (
      <React.Fragment>
        <div className="main-loader">
          <Loading
            show={this.state.loading}
            color="teal"
            showSpinner={false}
          />
        </div>
        {getTotalDiv()}
      </React.Fragment>
    );
  }
}

export default withRouter(Research);
