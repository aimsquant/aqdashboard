import React, { Component } from 'react';
import Utils from './../../Utils';
import axios from 'axios';
import { Icon, Input, Form, Button, Row, Col, Spin } from 'antd';
import { withRouter } from 'react-router-dom';


class NewStartegy extends Component {

  _mounted = false;

  constructor(props){
  	super();
    let strategy = {
      'name':'',
      'description':'',
      'code':'',
      'language': 'julia',
      'type':''
    };
    if (props.startegyClone){
      strategy = JSON.parse(JSON.stringify(props.startegyClone));
      strategy.name = strategy.name+"_C";
    }
  	this.state = {
      'loading': false,
      'strategy': strategy
  	};
    this.updateState = (data) => {
      if (this._mounted){
        this.setState(data);
      }
    }

    this.strategyNameChange = (e) => {
      this.updateState({
        'strategy': {
          'name': e.target.value,
          ...this.state.strategy
        }
      })
    }

    this.strategyDescriptionChange = (e) => {
      this.updateState({
        'strategy': {
          'description': e.target.value,
          ...this.state.strategy
        }
      })
    }

    this.handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.updateState({'loading': true});
          axios({
              method: 'POST',
              url: Utils.getBaseUrl() + '/strategy',
              data:{
                'name': values.strategyName,
                'language': this.state.strategy.language,
                'description': values.shortDescription,
                'code': this.state.strategy.code,
                'type': this.state.strategy.type
              },
              'headers': Utils.getAuthTokenHeader()
            })
          .then((response) => {
              this.props.history.push('/research/strategy/'+response.data._id);
          })
          .catch((error) => {
            Utils.checkForInternet(error, this.props.history);
            if (error.response) {
              if (error.response.status === 400 || error.response.status === 403) {
                this.props.history.push('/forbiddenAccess');
              }
              Utils.checkErrorForTokenExpiry(error, this.props.history, this.props.match.url);
            }
            this.updateState({
              'loading': false
            });
          });
        }
      });
    }

  }

  componentDidMount(){
    this._mounted = true;
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  render() {

    const antIconLoading = <Icon type="loading" style={{ fontSize: 34 }} spin />;
    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;
    const { TextArea } = Input;

    const getButtonsDiv = () => {
      if (this.state.loading){
        return (
           <div style={{'display': 'flex',
            'alignItems': 'center', 'justifyContent': 'center',
            'minWidth': '300px'}}>
            <Spin indicator={antIconLoading} />
          </div>
        );
      }else{
        return (
          <Row>
            <Col span={12}>
              <Button className="login-form-button"
                style={{'marginRight': '10px'}} onClick={this.props.onCancel}>
                Cancel
              </Button>
            </Col>
            <Col span={12}>
              <Button type="primary" htmlType="submit" className="login-form-button"
                style={{'marginLeft': '10px'}}>
                Create
              </Button>
            </Col>
          </Row>
        );
      }
    }

    return (
      <div className="new-strategy-div" style={{'padding': '20px'}}>
        <h2>CREATE STRATEGY</h2>
        <Form onSubmit={this.handleSubmit}
        style={{'margin':'0 auto'}}>
          <FormItem>
            {getFieldDecorator('strategyName', {
              rules: [{ required: true, message: 'Please input startegy name!' },
              {'max': 30, message: 'Name cannot be longer than 30 characters'}],
              initialValue: this.state.strategy.name
            })(
              <Input placeholder="Strategy Name"
                onChange={this.strategyNameChange} />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('shortDescription', {
              rules: [{ required: true, message: 'Please input short description!' },
              {'max': 60, message: 'Description cannot be longer than 60 characters'}],
               initialValue: this.state.strategy.description
            })(
              <TextArea placeholder="Short Description"
                onChange={this.strategyDescriptionChange}/>
            )}
          </FormItem>
          <FormItem>
            {getButtonsDiv()}
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default withRouter(Form.create()(NewStartegy));
