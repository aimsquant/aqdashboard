import React, { Component } from 'react';
import { Button, Row, Col, Icon } from 'antd';
import {Link, withRouter} from 'react-router-dom';
import ReactDOM from 'react-dom';


class Home extends Component {

  constructor(props){
  	super();
  	this.state = {

  	};

    this.handleScrollToElement = (key) =>{
      const tesNode = ReactDOM.findDOMNode(this.refs[key])
      if (tesNode){
        window.scrollTo(0, tesNode.offsetTop);
      }
    }
  }

  componentDidMount(){
  	if (this.props.pageChange){
  		this.props.pageChange('home');
  	}
  }

  render() {


    return (
	    <div>
        <div className="full-screen-container" style={{'textAlign': 'center'}}>
          <p className="invest-heading">Invest in Your Ideas</p>
          <h2 style={{'margin': '0px 0px 4% 0px'}}>Systematically research investment strategies in seconds</h2>
          <div>
              <Link to='/signup'><Button type="primary" className="register-button">Register</Button></Link>
              <Button onClick={() => {this.handleScrollToElement('moreInfo')}} className="learn-more-button">Learn More</Button>
          </div>
          <Icon className="pointer-hover" onClick={() => {this.handleScrollToElement('moreInfo')}} type="down" 
          style={{'position': 'absolute', 'bottom': '84px', 'fontWeight': 'bolder',
          'fontSize': '20px'}}/>
        </div>
        <div ref="moreInfo" style={{'padding': '10%', 'background': '#fafafa'}}>
          <Row>
            <Col xs={24} sm={24} md={12} lg={12} xl={12} style={{'textAlign': 'center',
              'height': '50vh'}}>
              <div className="height_width_full center-content">
                <img className="more-info-image" src="./assets/images/research-big.png" alt="Research" />
                <div className="more-info-text">
                  <h2>RESEARCH</h2>
                  <h4>Build, test and research investment stategies with in-built code editor and data from Indian Equity markets since 2007</h4>
                </div>
              </div>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={12} style={{'textAlign': 'center',
              'height': '50vh'}}>
              <div className="height_width_full center-content">
                <img className="more-info-image" src="./assets/images/analyze-big.png" alt="Analyze" />
                <div className="more-info-text">
                  <h2>ANALYZE</h2>
                  <h4>Run unlimited backtests and visualize performance of investment strategies with variety of metrics</h4>
                </div>
              </div>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={12} style={{'textAlign': 'center',
              'height': '50vh'}}>
              <div className="height_width_full center-content">
                <img className="more-info-image" src="./assets/images/community-big.png" alt="Community" />
                <div className="more-info-text">
                  <h2>COMMUNITY</h2>
                  <h4>Share your investment insights with the community, learn from the feedback and continuously improve your investment strategies</h4>
                </div>
              </div>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={12} style={{'textAlign': 'center',
              'height': '50vh'}}>
              <div className="height_width_full center-content">
                <img className="more-info-image" src="./assets/images/trade.png" alt="Trade" />
                <div className="more-info-text">
                  <h2>TRADE</h2>
                  <h4>Forward simulate your investment ideas, track the peformance and portfolio and use the output to trade in real markets</h4>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div style={{'textAlign': 'center', 'padding': '10%', 'background': '#e8f7f8'}}>
          <p className="research-heading">Start researching your investment ideas now</p>
          <div>
              <Link to='/signup'><Button type="primary" className="register-button">Join Now</Button></Link>
          </div>
        </div>
	    </div>
    );
  }
}

export default withRouter(Home);
