import React, { Component } from 'react';
import './App.css';
import { Switch, Route, Link,withRouter, Redirect } from 'react-router-dom';
import { Layout, Menu, Popover, Button, Icon, Modal, message } from 'antd';
import axios from 'axios';
import Utils from './Utils';
import Home from './Home/Home.jsx';
import AboutUs from './AboutUs/AboutUs.jsx';
import Community from './Community/Community.jsx';
import Login from './Login/Login.jsx';
import ForgotPassword from './ForgotPassword/ForgotPassword.jsx';
import Signup from './Signup/Signup.jsx';
import Policy from './Policy/Policy.jsx';
import TnC from './Policy/TnC.jsx';
import ThreadView from './Community/ThreadView/ThreadView.jsx';
import AuthMessage from './AuthMessage/AuthMessage.jsx';
import NewPost from './Community/NewPost/NewPost.jsx';
import Help from './Help/Help.jsx';  
import logo from './assets/Logo.png';
import Research from './Research/Research.jsx';
import StrategyBacktests from './Research/StrategyBacktests/StrategyBacktests.jsx';
import BacktestDetail from './Research/BacktestDetail/BacktestDetail.jsx';
import ForwardtestDetail from './Research/ForwardtestDetail/ForwardtestDetail.jsx';
import StartegyDetail from './Research/StartegyDetail/StartegyDetail.jsx';
import InviteFriend from './InviteFriend/InviteFriend.jsx';
import SendFeedback from './SendFeedback/SendFeedback.jsx';
import TokenUpdate from './TokenUpdate/TokenUpdate.jsx';
import {ForbiddenAccess, NoIternetAccess, PageNotFound} from './ErrorPages';

const regexArray = [
  {regExp: '^\/errorPage$', title: 'Error - AimsQuant'},
  {regExp: '^\/aboutUs$', title: 'Error - AimsQuant'},
  {regExp: '^\/people$', title: 'People - AimsQuant'},
  {regExp: '^\/connect$', title: 'Connect - AimsQuant'},
  {regExp: '^\/careers$', title: 'Careers - AimsQuant'},
  {regExp: '^\/login$', title: 'Login - AimsQuant'},
  {regExp: '^\/signup$', title: 'Signup - AimsQuant'},
  {regExp: '^\/forgotPassword$', title: 'Forgot Password - AimsQuant'},
  {regExp: '^\/policy\/policy$', title: 'Privacy Policy - AimsQuant'},
  {regExp: '^\/policy\/tnc$', title: 'Terms and Conditions - AimsQuant'},
  {regExp: '^\/community\/postDetail\/[A-Za-z0-9]+$', title: 'Post Detail - AimsQuant'},
  {regExp: '^\/community\/newPost$', title: 'New Post - AimsQuant'},
  {regExp: '^\/community$', title: 'Community - AimsQuant'},
  {regExp: '^\/research\/backtests\/[A-Za-z0-9]+$', title: 'Backtests - AimsQuant'},
  {regExp: '^\/research\/backtests\/[A-Za-z0-9]+\/[A-Za-z0-9]+$', title: 'Backtest - AimsQuant'},
  {regExp: '^\/research\/forwardtest\/[A-Za-z0-9]+\/[A-Za-z0-9]+$', title: 'Forwardtest - AimsQuant'},
  {regExp: '^\/research\/strategy\/[A-Za-z0-9]+$', title: 'Forwardtest - AimsQuant'},
  {regExp: '^\/research$', title: 'Research - AimsQuant'},
  {regExp: '^\/authMessage$', title: 'Authentication Message - AimsQuant'},
  {regExp: '^\/help$', title: 'Help - AimsQuant'},
  {regExp: '^\/home$', title: 'Home - AimsQuant'},
  {regExp: '^\/home$', title: 'Home - AimsQuant'},
];



class App extends Component {

  inviteFriendEmail = "";
  feedbackTitle = "Feedback or Feature Request";
  feedbackText = "";

  constructor(props){
    super();
    this.state={
      'currentPage': '',
      'popupVisible': false,
      'showInviteFriendModal': false,
      'showFeedbackModal': false
    };

    this.pageChange = (currentPage) =>{
      // if (this.state.currentPage !== currentPage){
      //   this.setState({'currentPage': currentPage});
      // }
    } 

    this.inviteFriendEmailchange = (value) => {
      this.inviteFriendEmail = value;
    }

    this.inviteFriend = () => {
      Modal.confirm({
        title: 'Invite a Friend',
        content: <InviteFriend inviteFriendEmailchange={this.inviteFriendEmailchange}/>,
        okText: 'Send',
        okType: 'primary',
        cancelText: 'Cancel',
        className: 'invite-friend-modal',
        iconType: 'smile',
        onOk: () => {
          axios({
              method: 'POST',
              url: Utils.getBaseUrl() + '/user/sendInvite',
              data:{
                'emailList': this.inviteFriendEmail
              },
              'headers': Utils.getAuthTokenHeader()
            })
          .then((response) => {
              message.success('Invite sent to ' + this.inviteFriendEmail);
              this.inviteFriendEmail = "";
          })
          .catch((error) => {
            
          });
        },
        onCancel: () => {
          this.inviteFriendEmail = "";
        },
      });
    }

    this.feedbackTitleChange = (value) => {
      this.feedbackTitle = value;
    }

    this.feedbackTextChange = (value) => {
      this.feedbackText = value;
    }

    this.sendFeedback = () =>{
      Modal.confirm({
        title: 'Send Feedback',
        content: <SendFeedback feedbackTitleChange={this.feedbackTitleChange}
          feedbackTextChange={this.feedbackTextChange}/>,
        okText: 'Send',
        okType: 'primary',
        cancelText: 'Cancel',
        className: 'invite-friend-modal',
        width: 600,
        iconType: 'right-circle-o',
        onOk: () => {
          axios({
              method: 'POST',
              url: Utils.getBaseUrl() + '/user/sendFeedback',
              data:{
                'feedback': this.feedbackText,
                'subject': this.feedbackTitle
              },
              'headers': Utils.getAuthTokenHeader()
            })
          .then((response) => {
              message.success('Thanks for sharing your valuable feedback.');
              this.feedbackText = "";
              this.feedbackTitle = "";
          })
          .catch((error) => {
            
          });
        },
        onCancel: () => {
          this.inviteFriendEmail = "";
        },
      });
    }

    this.logout = () =>{
      Utils.logoutUser();
      this.props.history.push('/login');
    }

  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) { // Route changed
        this.onRouteChanged(this.props.location.pathname);
    }
  }

  componentDidMount() {
    const location = this.props.location.pathname;
    this.setPageTitle(location);
  }

  onRouteChanged = location => {
    this.setPageTitle(location);
  }

  setPageTitle = location => {
    regexArray.map((item, index) => {
      const expression = new RegExp(item.regExp);
      if (expression.test(location)) {
        document.title = item.title;
        return;
      }
    })
  }

  render() {

  	const { Header, Content, Footer } = Layout;

    const getClassName = (key) =>{
      if (this.state.currentPage === key ||
        (this.props.location && this.props.location.pathname &&
        this.props.location.pathname.startsWith('/'+key))){
        return 'ant-menu-item-selected';
      }else if((key === 'research' || key === 'home') && (
        !this.props.location || !this.props.location.pathname ||
        this.props.location.pathname === '/' ||
        Utils.getLowerCasedNoSpaces(this.props.location.pathname) === '')){
        return 'ant-menu-item-selected';
      }else{
        return '';
      }
    }

  	const getHeaderMenuItems = () => {
  		if (Utils.isLoggedIn()){
        const popUpContent = (
          <div>
            <div className="loggedinuser-menu-popup-header">
              <div>
                <h3>{Utils.getLoggedInUserName()}</h3>
                <p>{Utils.getLoggedInUserEmail()}</p>
              </div>
            </div>
            <div className="loggedinuser-menu-popup-content">
              <div className="row" onClick={this.inviteFriend}>
                <Icon type="user-add" className="icon" />
                INVITE A FRIEND
              </div>
              <div className="row" onClick={this.sendFeedback}>
                <Icon type="message" className="icon" />
                SEND US FEEDBACK
              </div>
              <div className="row" onClick={this.logout}>
                <Icon type="logout" className="icon" />
                SIGN OUT
              </div>
            </div>
          </div>
        );
  			const HeaderMenu = withRouter(({ history }) => (
            <Menu
              mode="horizontal"
              className="menu"
              onClick={(e) => { if(e.key !== 'user') { history.push('/'+e.key) } }}
            >
              <Menu.Item className={"menu-item " + getClassName('research')} key="research">RESEARCH</Menu.Item>
              <Menu.Item className={"menu-item " + getClassName('community')} key="community">COMMUNITY</Menu.Item>
              <Menu.Item className={"menu-item " + getClassName('help')} key="help">HELP</Menu.Item>
              <Menu.Item className={"menu-item "} key="user">
                <Popover placement="bottomRight" content={popUpContent} trigger="click">
                  <Button className="logged-in-profile-pic">{Utils.getLoggedInUserInitials()}</Button>
                </Popover>
              </Menu.Item>
            </Menu>
          ));
        return(
          <HeaderMenu />
        );
  		}else{
        const HeaderMenu = withRouter(({ history }) => (
            <Menu
              mode="horizontal"
              className="menu"
              onClick={(e) => { history.push('/'+e.key) }}
            >
              <Menu.Item className={"menu-item " + getClassName('home')} key="home">HOME</Menu.Item>
              <Menu.Item className={"menu-item " + getClassName('aboutUs')} key="aboutUs">ABOUT US</Menu.Item>
              <Menu.Item className={"menu-item " + getClassName('community')} key="community">COMMUNITY</Menu.Item>
              <Menu.Item className={"menu-item " + getClassName('login')} key="login">LOGIN</Menu.Item>
              <Menu.Item className={"menu-item signup " + getClassName('signup')} key="signup">SIGN UP</Menu.Item>
            </Menu>
          ));
  			return(
          <HeaderMenu />
  			);
  		}
  	}

    const getSmallHeaderMenuItems = () => {
      if (Utils.isLoggedIn()){
        const getPopUpContent = (history) => {
          return (
            <div>
              <div className="loggedinuser-menu-popup-header">
                <div>
                  <h3>{Utils.getLoggedInUserName()}</h3>
                  <p>{Utils.getLoggedInUserEmail()}</p>
                </div>
              </div>
              <div className="loggedinuser-menu-popup-content">
                <div className="row" onClick={() => history.push('/research')}>
                  RESEARCH
                </div>
                <div className="row" onClick={() => history.push('/community')}>
                  COMMUNITY
                </div>
                <div className="row" onClick={() => history.push('/help')}>
                  HELP
                </div>
                <div className="row" onClick={this.inviteFriend}>
                  <Icon type="user-add" className="icon" />
                  INVITE A FRIEND
                </div>
                <div className="row" onClick={this.sendFeedback}>
                  <Icon type="message" className="icon" />
                  SEND US FEEDBACK
                </div>
                <div className="row" onClick={this.logout}>
                  <Icon type="logout" className="icon" />
                  SIGN OUT
                </div>
              </div>
            </div>
          );
        }
        const HeaderMenu = withRouter(({ history }) => (
            <Menu
              mode="horizontal"
              className="menu"
            >
              <Menu.Item className={"menu-item "} key="user">
                <Popover placement="bottomRight" content={getPopUpContent(history)} trigger="click">
                  <Button className="logged-in-profile-pic">{Utils.getLoggedInUserInitials()}</Button>
                </Popover>
              </Menu.Item>
            </Menu>
          ));
        return(
          <HeaderMenu />
        );
      }else{
        const getPopUpContent = (history) => {
          return (
            <div>
              <div className="loggedinuser-menu-popup-content">
                <div className="row" onClick={() => history.push('/home')}>
                  HOME
                </div>
                <div className="row" onClick={() => history.push('/aboutUs')}>
                  ABOUT US
                </div>
                <div className="row" onClick={() => history.push('/community')}>
                  COMMUNITY
                </div>
                <div className="row" onClick={() => history.push('/login')}>
                  LOGIN
                </div>
                <div className="row" onClick={() => history.push('/signup')}>
                  SIGN UP
                </div>
              </div>
            </div>
          );
        }
        const HeaderMenu = withRouter(({ history }) => (
            <Menu
              mode="horizontal"
              className="menu"
            >
              <Menu.Item className={"menu-item "} key="user">
                <Popover placement="bottomRight" content={getPopUpContent(history)} trigger="click">
                  <Button ><Icon style={{'margin': '0px'}} type="menu-unfold" className="icon" /></Button>
                </Popover>
              </Menu.Item>
            </Menu>
          ));
        return(
          <HeaderMenu />
        );
      }
    }

  	const getFooter = () =>{
      if(!Utils.isLoggedIn() && (this.state.currentPage === 'home' ||
        this.state.currentPage === 'aboutUs')){
        return(
          <Footer className="footer">
            <div>
            	<h2>Company</h2>
            	<Link to='/aboutUs'><p>About Us</p></Link>
            	<Link to='/people'><p>People</p></Link>
            	<Link to='/careers'><p>Careers</p></Link>
            	<Link to='/connect'><p>Connect</p></Link>
            </div>
            <div style={{'marginLeft': '20px'}}>
            	<h2>Policies</h2>
            	<Link to='/policy/tnc'><p>Terms of Use</p></Link>
            	<Link to='/policy/policy'><p>Privacy Policy</p></Link>
            </div>
            <div style={{'margin': 'auto 0px 0px auto'}}>
            	<h4 style={{'color': 'white'}}>&#169;2018 AimsQuant</h4>
            </div>
          </Footer>
        );
      }
    }

    const aboutUsComponent = (props) =>{
      return (
        <AboutUs
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const homeComponent = (props) =>{
      return (
        <Home
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const signupComponent = (props) =>{
      return (
        <Signup
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const loginComponent = (props) =>{
      return (
        <Login
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const communityComponent = (props) =>{
      return (
        <Community
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const postDetailComponent = (props) =>{
      return (
        <ThreadView
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const authMessageComponent = (props) =>{
      return (
        <AuthMessage
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const newPostComponent = (props) =>{
      return (
        <NewPost
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const helpComponent = (props) =>{
      return (
        <Help
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const researchComponent = (props) =>{
      return (
        <Research
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const strategyBacktestsComponent = (props) => {
      return (
        <StrategyBacktests
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const BacktestDetailComponent = (props) => {
      return (
        <BacktestDetail
          pageChange = {this.pageChange}
          {...props}
        /> 
      );
    }

    const ForwardtestDetailComponent = (props) => {
      return (
        <ForwardtestDetail
          pageChange = {this.pageChange}
          {...props}
        />
      );
    }

    const startegyDetailComponent = (props) => {
      return (
        <StartegyDetail
          pageChange = {this.pageChange}
        />
      );
    }

    const getAppropriateComponent = () => {
      if (Utils.isLoggedIn()){
        return researchComponent;
      }else{
        return homeComponent;
      }
    }

    return (
      <Layout className="layout">
  	    <Header className="header big-screen">
  	      <div className="logo" style={{fontSize: '22px'}}>
  	      	<h3 className="aims">Aims</h3>
  	      	<h3 className="quant">Quant</h3>
  	      </div>
          <img alt="" style={{'height': '35px', 'width': 'auto'}} src={logo} />
  	      {getHeaderMenuItems()}
  	    </Header>
        <Header className="header small-screen">
          <div className="logo">
            <h3 className="aims">Aims</h3>
            <h3 className="quant">Quant</h3>
          </div>
          {getSmallHeaderMenuItems()}
        </Header>
  	    <Content className="content">
  	      <Switch>
            <Route path='/aboutUs' component={aboutUsComponent}/>
            <Route path='/people' component={aboutUsComponent}/>
            <Route path='/connect' component={aboutUsComponent}/>
            <Route path='/careers' component={aboutUsComponent}/>
            <Route path='/login' component={loginComponent}/>
            <Route path='/signup' component={signupComponent}/>
            <Route path='/forgotPassword' component={ForgotPassword}/>
            <Route path='/policy/policy' component={Policy}/>
            <Route path='/policy/tnc' component={TnC}/>
            <Route exact path='/community/postDetail/:postId' component={postDetailComponent} />
            <Route exact path='/community/newPost' component={newPostComponent}/>
            <Route path='/community' component={communityComponent}/>
            <Route exact path='/research/backtests/:strategyId' component={strategyBacktestsComponent}/>
            <Route exact path='/research/backtests/:strategyId/:backtestId' component={BacktestDetailComponent} />
            <Route exact path='/research/forwardtest/:strategyId/:forwardtestId' component={ForwardtestDetailComponent} />
            <Route exact path='/research/strategy/:strategyId' component={startegyDetailComponent} />
            <Route path='/research' component={researchComponent}/>
            <Route path='/authMessage' component={authMessageComponent}/>
            <Route path='/help' component={helpComponent}/>
            <Route path='/home' component={homeComponent}/>
            <Route path='/tokenUpdate' component={TokenUpdate}/>
            <Route exact path='/forbiddenAccess' component={ForbiddenAccess} />
            <Route exact path='/errorPage' component={NoIternetAccess} />
            <Route exact path='/' component={getAppropriateComponent()}/>
            <Redirect from="*" to="/" />
          </Switch>
  	    </Content>
  	    {getFooter()}
	    </Layout>
    );
  }
}

export default withRouter(App);
