import React, { Component } from 'react';
import { Icon } from 'antd';
import {withRouter} from 'react-router-dom';
import Utils from './../../Utils';

class Pagination extends Component {

  _mounted = false;

  constructor(props){
  	super();
  	this.state = {
  	};


    this.clickedOnFirstPage = () =>{
      if (Utils.isLoggedIn()){
        if (this.props.page !== 1){
          this.props.onpageChanged(1);
        }
      }else{
        Utils.goToLoginPage(this.props.history, this.props.match.url);
      }
    }

    this.clickedOnLastPage = () =>{
      if (Utils.isLoggedIn()){
        let currentPage = parseInt(JSON.stringify(this.props.page), 10);
        if (currentPage !== this.props.numberOfPages){
          this.props.onpageChanged(this.props.numberOfPages);
        }
      }else{
        Utils.goToLoginPage(this.props.history, this.props.match.url);
      }
    }

    this.clickedOnPrevPage = () =>{
      if (Utils.isLoggedIn()){
        let currentPage = parseInt(JSON.stringify(this.props.page), 10);
        if (currentPage > 1){
          this.props.onpageChanged(currentPage - 1);
        }
      }else{
        Utils.goToLoginPage(this.props.history, this.props.match.url);
      }
    }

    this.clickedOnNextPage = () =>{
      if (Utils.isLoggedIn()){
        let currentPage = parseInt(JSON.stringify(this.props.page), 10);
        if ((currentPage + 1) <= this.props.numberOfPages){
          this.props.onpageChanged(currentPage + 1);
        }
      }else{
        Utils.goToLoginPage(this.props.history, this.props.match.url);
      }
    }

    this.updateState = (data) => {
      if (this._mounted){
        this.setState(data);
      }
    }

  }


  componentDidMount(){
    this._mounted = true;
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  render() {

    return (
         <div style={{'justifyContent': 'center', 'alignItems': 'center',
           'display': 'flex', 'marginTop': '15px'}}>
          <Icon onClick={this.clickedOnFirstPage} type="double-left" style={{'color': (this.props.page <= 1) ? '#9b9b9b' : 'black', 'fontWeight': 'bold',
            'fontSize': '20px', 'padding': '5px', 'cursor': 'pointer'}} />
          <Icon onClick={this.clickedOnPrevPage} type="left" style={{'color': (this.props.page <= 1) ? '#9b9b9b' : 'black', 'fontWeight': 'bold',
            'fontSize': '20px', 'padding': '5px', 'cursor': 'pointer'}} />
          <h3 style={{'minWidth': '100px', 'textAlign':'center',
            'color': '#3c3c3c', 'margin': '0px'}}>PAGE {this.props.page}</h3>
          <Icon onClick={this.clickedOnNextPage} type="right" style={{'color': (this.props.numberOfPages <= this.props.page) ? '#9b9b9b' : 'black', 'fontWeight': 'bold',
            'fontSize': '20px', 'padding': '5px', 'cursor': 'pointer'}} />
          <Icon onClick={this.clickedOnLastPage} type="double-right" style={{'color': (this.props.numberOfPages <= this.props.page) ? '#9b9b9b' : 'black', 'fontWeight': 'bold',
            'fontSize': '20px', 'padding': '5px', 'cursor': 'pointer'}} />
         </div>
    );
  }
}

export default withRouter(Pagination);
