import React, { Component } from 'react';
import {Tabs } from 'antd';
import Utils from './../../Utils';
import {withRouter} from 'react-router-dom';

class CommunityTabs extends Component {

  constructor(props){
  	super();
  	this.state = {};
  }

  render() {

    const TabPane = Tabs.TabPane;
    const tabs = [];

    tabs.push(<TabPane tab="Popular" key="popular" style={{'height':'calc(100% - 60px)'}}>
          </TabPane>);
    tabs.push(<TabPane tab="Newest" key="newest" style={{'height':'calc(100% - 60px)'}}>
          </TabPane>);
    if (Utils.isLoggedIn()){
      tabs.push(<TabPane tab="Following" key="following" style={{'height':'calc(100% - 60px)'}}>
          </TabPane>);
      tabs.push(<TabPane tab="Personal" key="personal" style={{'height':'calc(100% - 60px)'}}>
          </TabPane>);
    }

    return (
        <Tabs className="height_width_full community-tabs" animated={false} onChange={this.props.onTabChanged}
          activeKey={this.props.selectedTabValue}>
          {tabs}
        </Tabs>
    );
  }
}

export default withRouter(CommunityTabs);
