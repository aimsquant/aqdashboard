import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.jsx';
import registerServiceWorker from './registerServiceWorker';
import { Switch, Route, BrowserRouter } from 'react-router-dom'

ReactDOM.render(
	<BrowserRouter>
	    <Switch>
		  <Route path='/' component={App}/>
		</Switch>
	</BrowserRouter>
, document.getElementById('root'));

registerServiceWorker();
