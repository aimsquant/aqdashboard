module.exports = {
    requestUrl: 'https://developapi.aimsquant.com/api/v2',
    webSocketUrl: 'wss://developapi.aimsquant.com'
};