import React, { Component } from 'react';
import { Input, Form } from 'antd';


class InviteFriend extends Component {

  _mounted = false;

  constructor(props){
  	super();
  	this.state = {
  	};
    this.updateState = (data) => {
      if (this._mounted){
        this.setState(data);
      }
    }

    this.emailchange = (e) => {
      if (this.props.inviteFriendEmailchange){
        this.props.inviteFriendEmailchange(e.target.value);
      }
    }

  }

  componentDidMount(){
    this._mounted = true;
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  render() {

    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;

    return (
      <div className="new-strategy-div" style={{'padding': '20px',
        'textAlign':'center'}}>
        <p>We are thrilled you like our platform.</p>
        <p>Spread the joy!!!</p>
        <Form
        style={{'margin':'0 auto'}}>
          <FormItem>
            {getFieldDecorator('email', {
              rules: [{
                type: 'email', message: 'The input is not valid E-mail!',
              }, {
                required: true, message: 'Please input your E-mail!',
              }],
            })(
              <Input placeholder="Email"
                onChange={this.emailchange}/>
            )}
          </FormItem>
        </Form>
      </div>
    );
  }
}

export default (Form.create()(InviteFriend));
