import React, { Component } from 'react';
import {Button} from 'antd';
import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';

class AboutUs extends Component {

  constructor(props){
  	super();
  	this.state = {

  	};

    this.handleScrollToElement = (key) =>{
      const tesNode = ReactDOM.findDOMNode(this.refs[key])
      if (tesNode){
        window.scrollTo(0, tesNode.offsetTop);
      }
    }
  }

  componentDidMount(){
  	if (this.props.pageChange){
  		this.props.pageChange('aboutUs');
  	}

    if (this.props.location){
      if (this.props.location.pathname === '/people'){
        setTimeout(() =>{
          this.handleScrollToElement('whoWeAre');
        }, 100);
      }else if (this.props.location.pathname === '/careers'){
        setTimeout(() =>{
          this.handleScrollToElement('careers');
        }, 100);
      }else if (this.props.location.pathname === '/connect'){
        setTimeout(() =>{
          this.handleScrollToElement('connectWithUs');
        }, 100);
      }else{
        setTimeout(() =>{
          this.handleScrollToElement('aboutUs');
        }, 100);
      }
    }

  }

  render() {


    return (
	    <div>
        <div ref="aboutUs" className="full-screen-container" 
          style={{'background': 'white', 'padding': '4% 10% 4% 10%'}}>
          <h1 style={{'fontSize': 'calc(8px + 1.5vw)', 'fontWeight': 'bolder'}}>About Us</h1>
          <p style={{'fontSize':'calc(7px + 1.5vw)', 'color': 'teal'}}>
            Invest in your Ideas with tools of professionals
          </p>
          <div style={{'display': 'inline-flex', 'alignItems': 'center', 'padding': '2% 6% 0 6%'}}>
              <img className="link-image" src="./assets/images/link.png" alt="Link" />
              <div className="link-text">
                <h4>AimsQuant aims to bridge the data and technology gap between investment 
                enthusiasts and great investments. We want to create an ecosystem 
                where data scientists, quantitative analysts, programmers and investment 
                enthusiasts come together to create the best investment ideas.</h4>
                <div style={{'paddingTop': '15px'}}>
                    <Button onClick={() => {this.handleScrollToElement('whatWeBuild')}} className="register-button">Read More</Button>
                </div>
              </div>
          </div>
        </div>
        <div ref="whatWeBuild" className="full-screen-container" 
          style={{'background': 'white', 'padding': '4% 10% 4% 10%'}}>
          <h1 style={{'fontSize': 'calc(8px + 1.5vw)', 'fontWeight': 'bolder', 'marginTop': '10%'}}>What are we building</h1>
          <p style={{'fontSize':'calc(7px + 1.5vw)', 'color': 'teal'}}>
            Quantitative Research Platform
          </p>
          <div style={{'display': 'inline-flex', 'alignItems': 'center'}}>
              <div className="link-text" style={{'padding': '0px'}}>
                <h4 style={{'marginTop': '10px'}}>In ever expanding financial markets it is important to systematically scrutinize investment ideas and separate real investment signals from noise. At AimsQuant, we are building an easy to use web platform to systematically research investment ideas.</h4>
                <h4 style={{'marginTop': '20px'}}>With access to superior tools and free data, we want to democratize the investment arena. At AimsQuant, anyone can build and test investment strategies, share it with the community for the feeback or trade it on personal account.</h4>
                <div style={{'paddingTop': '25px'}}> 
                    <Button onClick={() => {this.handleScrollToElement('whoWeAre')}} className="register-button">Read More</Button>
                </div>
              </div>
          </div>
        </div>
        <div ref="whoWeAre" className="full-screen-container" 
          style={{'background': 'white', 'padding': '4% 10% 4% 10%'}}>
          <h1 style={{'fontSize': 'calc(8px + 1.5vw)', 'fontWeight': 'bolder', 'marginTop': '10%'}}>Who We Are</h1>
          <p style={{'fontSize':'calc(7px + 1.5vw)', 'color': 'teal'}}>
            They call us "Quants"
          </p>
          <div style={{'display': 'inline-flex', 'alignItems': 'center'}}>
              <div className="link-text" style={{'padding': '0px'}}>
                <h4 style={{'marginTop': '20px'}}>We are bunch of Quants and Technologists who enjoy financial markets and making efficient technology. We aim to simplify investment science and filter noise from real investment ideas.</h4>
                <div style={{'paddingTop': '25px'}}> 
                    <Button onClick={() => {this.handleScrollToElement('careers')}} className="register-button">Read More</Button>
                </div>
              </div>
          </div>
        </div>
        <div ref="careers" className="full-screen-container" 
          style={{'background': 'white', 'padding': '4% 10% 4% 10%'}}>
          <h1 style={{'fontSize': 'calc(8px + 1.5vw)', 'fontWeight': 'bolder', 'marginTop': '10%'}}>Careers</h1>
          <p style={{'fontSize':'calc(7px + 1.5vw)', 'color': 'teal'}}>
            Those who see inefficiencies
          </p>
          <div style={{'display': 'inline-flex', 'alignItems': 'center'}}>
              <div className="link-text" style={{'padding': '0px'}}>
                <h4 style={{'marginTop': '20px'}}>We are always looking for highly motivated individuals interested in technology and financial markets. If you like to solve problems and build efficient technology, we would love to be in touch.</h4>
                <div style={{'paddingTop': '25px'}}> 
                    <a href='mailto:careers@aimsquant.com'><Button type="primary" className="register-button">APPLY FOR A JOB</Button></a>
                </div>
              </div>
          </div>
        </div>
        <div ref="connectWithUs" className="full-screen-container" 
          style={{'background': 'white', 'padding': '4% 10% 4% 10%'}}>
          <h1 style={{'fontSize': 'calc(8px + 1.5vw)', 'fontWeight': 'bolder', 'marginTop': '10%'}}>Connect With Us</h1>
          <p style={{'fontSize':'calc(7px + 1.5vw)', 'color': 'teal'}}>
            Like what we are building?
          </p>
          <div style={{'display': 'inline-flex', 'alignItems': 'center'}}>
              <div className="link-text" style={{'padding': '0px'}}>
                <h4 style={{'marginTop': '20px'}}>We always welcome insightful thoughts, great mentors and helpful suggestions. If you are an industry expert or an investor, we are eager to learn your opinion.</h4>
                <div style={{'paddingTop': '25px'}}> 
                    <a href='mailto:connect@aimsquant.com'><Button type="primary" className="register-button">CONTACT US</Button></a>
                </div>
              </div>
          </div>
        </div>
	    </div>
    );
  }
}

export default withRouter(AboutUs);
