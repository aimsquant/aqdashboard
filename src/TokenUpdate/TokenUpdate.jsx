import React, { Component } from 'react';
import Utils from './../Utils';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';


class TokenUpdate extends Component {

  redirectUrl = '/';

  constructor(props){
  	super();
  	this.state = {
  	};
    if(props.location.search){
      const queryParams = new URLSearchParams(props.location.search);
      if (queryParams && queryParams.get('redirectUrl')){
        this.redirectUrl = decodeURIComponent(queryParams.get('redirectUrl'));
        if (!this.redirectUrl){
          this.redirectUrl = '/';
        }
      }
    } 
    this.updateToken = () => {
      axios({
              method: 'PUT',
              url: Utils.getBaseUrl() + '/user/updateToken',
              data: {
                "email": Utils.getLoggedInUserEmail(),
                "token": Utils.getAuthToken()
              }
            })
          .then((response) => {
              Utils.setShouldUpdateToken(false);
              if(response.data && response.data.token){
                Utils.updateUserToken(response.data.token);
                if (this.redirectUrl){
                  this.props.history.push(this.redirectUrl);
                }else{
                  this.props.history.push('/research');
                }
              }else{
                Utils.logoutUser();
                this.props.history.push('/login');
              }
          })
          .catch((error) => {
             Utils.setShouldUpdateToken(false);
             Utils.logoutUser();
             this.props.history.push('/login');
          });
    }
  }

  componentDidMount(){
    this._mounted = true;
    // console.log(Utils.getShouldUpdateToken());
    if (Utils.getShouldUpdateToken() === 'true' ||
      Utils.getShouldUpdateToken() === true){
      this.updateToken();
    }else{
      this.props.history.push('/');
    }
  }

  componentWillUnMount(){
    this._mounted = false;
  }


  render() {
    return (
	    <div className="main-loader">
        <Loading
          show={true}
          color="teal"
          showSpinner={false}
        />
      </div>
    );
  }
}

export default (withRouter(TokenUpdate));
