import React, { Component } from 'react';
import { Input, Form } from 'antd';
import Utils from './../Utils';
import ReactQuill from 'react-quill';


class SendFeedback extends Component {

  _mounted = false;

  constructor(props){
  	super();
  	this.state = {
  	};
    this.updateState = (data) => {
      if (this._mounted){
        this.setState(data);
      }
    }

    this.feedbackTitleChange = (e) => {
      if (this.props.feedbackTitleChange){
        this.props.feedbackTitleChange(e.target.value);
      }
    }

    this.feedbackTextChange = (e) => {
      if (this.props.feedbackTextChange){
        this.props.feedbackTextChange(e);
      }
    }

  }

  componentDidMount(){
    this._mounted = true;
  }

  componentWillUnmount() {
    this._mounted = false;
  }

  render() {

    const { getFieldDecorator } = this.props.form;
    const FormItem = Form.Item;

    return (
      <div className="new-strategy-div" style={{'padding': '20px',
        'textAlign':'center'}}> 
        <Form
        style={{'margin':'0 auto'}}>
          <FormItem>
            {getFieldDecorator('feedbackTitle', {
              rules: [{
                required: true, message: 'Please enter Subject',
              }],
              initialValue: 'Feedback or Feature Request'
            })(
              <Input placeholder="Subject"
                onChange={this.feedbackTextChange}/>
            )}
          </FormItem>
          <ReactQuill style={{'marginTop': '10px'}} className="card qlEditor" value={this.state.markDownText}
                    onChange={this.feedbackTextChange} modules={Utils.getReactQuillEditorModules()} />
        </Form>
      </div>
    );
  }
}

export default (Form.create()(SendFeedback));
